
# DOMPET DIRUNA

Dompet Diruna merupakan Dompet Crypto-token Stellar yang khusus untuk mengelola Token Diruna (DRA)
Dibuat semata-mata karena Dompet Stellar yang ada tidak seperti yang saya inginkan (@ibnux)
Open source semata mata untuk belajar dan jadi referensi calon Developer lain.
Copyright (C) 2018 Ibnu Maksum

# Diruna Community Project
Adalah proyek yang dibangun untuk membantu mengatasi kesenjangan informasi tentang Crypto-token di lapisan masyarakat awam yang selama ini belum mengenal atau belum mengetahui secara mendalam bagaimana produk-produk yang dihasilkan dari teknologi Cryptography dan Blockchain sebenarnya mampu diterapkan dan memberikan manfaat bagi kehidupan seluruh umat manusia. Proyek ini juga diharapkan pada akhirnya akan mampu membantu meningkatkan jumlah pengguna Crypto-token yang saat ini prosentasenya masih sangat rendah jika dibandingkan dengan total penduduk di seluruh dunia.   Proyek ini kami targetkan untuk menyasar masyarakat awam yang sama sekali belum mengenal sedikitpun tentang dunia Crypto-token dan penerapannya di dalam kehidupan sehari-hari. Pendekatan yang digunakan dirancang sedemikian rupa agar proses pengenalan dan pembelajaran kepada masyarakat awam tersebut dapat berlangsung dengan mudah, menyenangkan, menguntungkan dan menumbuhkan kembali semangat kerjasama dan tolong-menolong di dalam diri setiap individu yang terlibat langsung dalam proyek ini.

Proses pengenalan dan pembelajaran dilakukan dengan membangun komunitaskomunitas dan membentuk Ekosistem Token Diruna di lingkungan-lingkungan tempat tinggal penduduk di setiap wilayah, kota ataupun negara. Ekosistem Token Diruna yang kami kembangkan terdiri atas tiga bagian, yaitu: Komunitas, Loyalty Point dan Marketplace Online . Kami meyakini bahwa proses pengenalan dan pembelajaran akan berlangsung lebih cepat dengan melibatkan setiap individu secara langsung ke dalam ekosistem.
[diruna.org](https://www.diruna.org/)


# License

**GNU GENERAL PUBLIC LICENSE**
Version 3, 29 June 2007

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

    Dompet Diruna  Copyright (C) 2018 Ibnu Maksum
    This program comes with ABSOLUTELY NO WARRANTY; for details type `show w'.
    This is free software, and you are welcome to redistribute it
    under certain conditions; type `show c' for details.
