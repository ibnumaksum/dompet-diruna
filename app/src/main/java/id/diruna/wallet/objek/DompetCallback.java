package id.diruna.wallet.objek;

public interface DompetCallback {

    void onDompetUpdated(Dompet dompet);
}
