package id.diruna.wallet.objek;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Pemilik extends RealmObject {
    @PrimaryKey
    public long idPemilik = 0;
    public int idDiruna;
    public String sessionDiruna;
    public String usernameDiruna;

    public String namaPemilik;
    public String nomorHP;
    public String email;
    public String urlWeb;
    public RealmList<Dompet> dompets;
    public byte[] foto; // persegi 80x80

    public Pemilik(){}

    public Pemilik(long idPemilik){
        this.idPemilik = idPemilik;
    }
}
