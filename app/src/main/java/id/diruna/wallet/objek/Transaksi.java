package id.diruna.wallet.objek;

import io.realm.RealmObject;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;

public class Transaksi  extends RealmObject {

    //operations
    @PrimaryKey
    public String transaction_hash;
    @Index
    public String publicKey;
    public String created_at,asset_type,asset_code;
    public String from,to,type;
    public Float amount;
    public long paging_tokenO = 0L;

    //transaction
    public String memo;
    public long paging_tokenT =0L;


    public Transaksi(){}

    public Transaksi(String transaction_hash){
        this.transaction_hash = transaction_hash;
    }

}
