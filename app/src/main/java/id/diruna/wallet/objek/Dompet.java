package id.diruna.wallet.objek;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Dompet extends RealmObject {

    @PrimaryKey
    public String publikKey;
    public String namaDompet;
    public String privateKeyDompet;
    public String inflation_destination;
    public String warnaDompet;
    public String warnaTeks;
    public String home_domain;
    public String sequence,subentry_count;
    public boolean isPrivate = false;
    public boolean isPrimary = false;
    public String saldos; //JsonArray

    public Dompet(){}

    public Dompet(String publikKeyDompet){
        this.publikKey = publikKeyDompet;
    }

    public Dompet(String namaDompet, String publikKeyDompet){
        this.namaDompet = namaDompet;
        this.publikKey = publikKeyDompet;
    }
}
