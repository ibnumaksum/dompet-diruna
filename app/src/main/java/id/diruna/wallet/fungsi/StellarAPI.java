package id.diruna.wallet.fungsi;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.ImageView;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.squareup.picasso.Picasso;

import id.diruna.wallet.objek.Dompet;
import id.diruna.wallet.objek.DompetCallback;

public class StellarAPI {
    private final String SERVER = "https://horizon.stellar.org/accounts/";
    private Context context;

    public StellarAPI(Context context){
        this.context = context;
    }

    public StellarAPI ambilDataDompet(String publicKey, FutureCallback<String> callback){
        Ion.with(context)
                .load(SERVER+publicKey)
                .setTimeout(10000)
                .setLogging("Diruna", Log.DEBUG)
                .asString()
                .setCallback(callback);
        return this;
    }

    public StellarAPI perbaruiSaldoDompet(final Dompet dompet, final DompetCallback callback){
        Ion.with(context)
                .load(SERVER+dompet.publikKey)
                .setTimeout(10000)
                .setLogging("Diruna", Log.DEBUG)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        if(e!=null && result==null){
                            callback.onDompetUpdated(dompet);
                        }else{
                            try {
                                dompet.home_domain = result.get("home_domain").getAsString();
                                dompet.sequence = result.get("sequence").getAsString();
                                dompet.subentry_count = result.get("subentry_count").getAsString();
                                dompet.saldos = result.get("balances").getAsJsonArray().toString();
                            }catch (Exception ee){}
                            callback.onDompetUpdated(dompet);
                        }
                    }
                });
        return this;
    }

    //Panggil operation dulu baru transaction
    public JsonObject getOperations(String publikKey,String paging_tokenO,int limit){
        try {
            return Ion.with(context)
                    .load(SERVER + publikKey + "/operations?cursor=" + paging_tokenO + "&limit=" + limit)
                    .setTimeout(10000)
                    .setLogging("Diruna", Log.DEBUG)
                    .asJsonObject()
                    .get();
            //.setCallback(callback);
        }catch (Exception e){
            Utils.log(e.getMessage());
            return null;
        }
    }

    public JsonObject getTransactions(String publikKey,long paging_tokenT,int limit){
        try {
            return Ion.with(context)
                    .load(SERVER + publikKey + "/transactions?cursor=" + paging_tokenT + "&limit=" + limit)
                    .setTimeout(10000)
                    .setLogging("Diruna", Log.DEBUG)
                    .asJsonObject()
                    .get();
            //.setCallback(callback);
        }catch (Exception e){
            Utils.log(e.getMessage());
            return null;
        }
    }

    public StellarAPI getKurs(final String kurs, final FutureCallback<String> callback){
        final String waktu = kurs+"TIME";
        SharedPreferences pref = Utils.getPrefKurs(context);
        long terakhir = pref.getLong(waktu,0);
        long sekarang = System.currentTimeMillis();
        long lama = 1800*1000;
        if(pref.getString(kurs,"").length()<1)
            lama = 0;
        if(sekarang-terakhir>lama) {
            Ion.with(context)
                    .load("https://diruna.id/data/"+kurs+".json")
                    .setTimeout(10000)
                    .setLogging("Diruna", Log.DEBUG)
                    .asString()
                    .setCallback(new FutureCallback<String>() {
                        @Override
                        public void onCompleted(Exception e, String result) {
                            Utils.log(result);
                            if(e!=null &&  e.getMessage()!=null &&  e.getMessage().length()>1){
                                callback.onCompleted(e,"-");
                            }else{
                                callback.onCompleted(e,result);
                                Utils.getPrefKurs(context).edit()
                                        .putString(kurs,result)
                                        .putLong(waktu,System.currentTimeMillis()).apply();
                            }
                        }
                    });
        }
        return this;
    }

    public StellarAPI getHourly(final FutureCallback<JsonArray> callback){
        final String waktu = "24jamTIME";
        SharedPreferences pref = Utils.getPrefKurs(context);
        long terakhir = pref.getLong(waktu,0);
        long sekarang = System.currentTimeMillis();
        if(sekarang-terakhir>3600*1000) {
            Ion.with(context)
                    .load("https://diruna.id/data/24jam.json")
                    .setTimeout(10000)
                    .setLogging("Diruna", Log.DEBUG)
                    .asJsonArray()
                    .setCallback(new FutureCallback<JsonArray>() {
                        @Override
                        public void onCompleted(Exception e, JsonArray result) {
                            if(e!=null &&  e.getMessage()!=null &&  e.getMessage().length()>1){
                                callback.onCompleted(null,new JsonParser().parse("[]").getAsJsonArray());
                            }else{
                                Utils.log(result.toString());
                                callback.onCompleted(e,result);
                                Utils.getPrefKurs(context).edit()
                                        .putString("24jam",result.toString())
                                        .putLong(waktu,System.currentTimeMillis()).apply();
                            }
                        }
                    });
        }else{
            String temp = pref.getString("24jam","[]");
            Utils.log(temp);
            callback.onCompleted(null,new JsonParser().parse(temp).getAsJsonArray());
        }
        return this;
    }

    public StellarAPI getImage(String url, ImageView img){
        Utils.log(url);
        Picasso.with(context)
                .load(url)
                //.placeholder(R.drawable.blank)
                //.error(R.drawable.blank)
                .into(img);
        return this;
    }

    public StellarAPI getImageResize(String url, ImageView img, int w, int h){
        Utils.log(url);
        Picasso.with(context)
                .load(url)
                .resize(w,h)
                //.placeholder(R.drawable.blank)
                //.error(R.drawable.blank)
                .centerCrop()
                .into(img);
        return this;
    }


}
