package id.diruna.wallet.fungsi;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import id.diruna.wallet.BuildConfig;
import id.diruna.wallet.R;

public class Utils {


    public static SharedPreferences getPrefKurs(Context context){
        return context.getSharedPreferences("wallet.kurs",0);
    }

    public static SharedPreferences getPref(Context context){
        return context.getSharedPreferences("wallet.pref",0);
    }


    public static String MD5(final String s) {
        log("MD5:"+s);
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString().toLowerCase();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static void log(String txt) {
        if(BuildConfig.DEBUG) {
            Log.d("DirunaWallet", "--------------------------------------");
            Log.d("DirunaWallet", txt + " ");
            Log.d("DirunaWallet", "--------------------------------------");
        }
    }

    public static void showToast(String pesan, Context c) {
        try {
            Toast t = Toast.makeText(c, pesan, Toast.LENGTH_LONG);
            t.setGravity(Gravity.CENTER,0,0);
            t.show();
        }catch (Exception e){
            log(e.toString());
        }

    }

    public static void showDialog(String pesan,Context context){
        showDialog(context.getString(R.string.app_name),pesan,context);
    }

    public static void showDialog(String judul, String pesan,Context context){
        new AlertDialog.Builder(context)
                .setIcon(R.mipmap.ic_launcher)
                .setTitle(judul)
                .setMessage(pesan)
                .setPositiveButton("OK", null)
                .show();
    }

    public static byte[] hexToBytes(String hex) {
        String HEXINDEX = "0123456789abcdef";
        int l = hex.length() / 2;
        byte data[] = new byte[l];
        int j = 0;

        for (int i = 0; i < l; i++) {
            char c = hex.charAt(j++);
            int n, b;

            n = HEXINDEX.indexOf(c);
            b = (n & 0xf) << 4;
            c = hex.charAt(j++);
            n = HEXINDEX.indexOf(c);
            b += (n & 0xf);
            data[i] = (byte) b;
        }

        return data;
    }

    public static String padString(String source) {
        char paddingChar = ' ';
        int size = 16;
        int padLength = size - source.length() % size;

        for (int i = 0; i < padLength; i++) {
            source += paddingChar;
        }

        return source;
    }

    public static String enkripsi(String text, String sandi)  throws NoSuchAlgorithmException, NoSuchPaddingException,
            InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException,
            BadPaddingException, IOException {
        int n = 0;
        while(sandi.length()<16){
            sandi += ""+n;
            n++;
            if(n>9){
                n = 0;
            }
        }
        log(sandi);
        byte[] clean = text.getBytes();

        // Generating IV.
        int ivSize = 16;
        byte[] iv = new byte[ivSize];
        SecureRandom random = new SecureRandom();
        random.nextBytes(iv);
        IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);

        // Hashing key.
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        digest.update(sandi.getBytes("UTF-8"));
        byte[] keyBytes = new byte[16];
        System.arraycopy(digest.digest(), 0, keyBytes, 0, keyBytes.length);
        SecretKeySpec secretKeySpec = new SecretKeySpec(keyBytes, "AES");

        // Encrypt.
        Cipher cipher = Cipher.getInstance("AES/GCM/NOPADDING");
        cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivParameterSpec);
        byte[] encrypted = cipher.doFinal(clean);

        // Combine IV and encrypted part.
        byte[] encryptedIVAndText = new byte[ivSize + encrypted.length];
        System.arraycopy(iv, 0, encryptedIVAndText, 0, ivSize);
        System.arraycopy(encrypted, 0, encryptedIVAndText, ivSize, encrypted.length);
        return Base64.encodeToString(encryptedIVAndText, Base64.DEFAULT);
    }

    public static String dekripsi(String encryptedText, String sandi) throws NoSuchAlgorithmException, NoSuchPaddingException,
            InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException,
            BadPaddingException, IOException {
        int n = 0;
        while(sandi.length()<16){
            sandi += ""+n;
            n++;
            if(n>9){
                n = 0;
            }
        }
        log(sandi);
        int ivSize = 16;
        int keySize = 16;
        byte[] encryptedIvTextBytes = Base64.decode(encryptedText, Base64.DEFAULT);
        // Extract IV.
        byte[] iv = new byte[ivSize];
        System.arraycopy(encryptedIvTextBytes, 0, iv, 0, iv.length);
        IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);

        // Extract encrypted part.
        int encryptedSize = encryptedIvTextBytes.length - ivSize;
        byte[] encryptedBytes = new byte[encryptedSize];
        System.arraycopy(encryptedIvTextBytes, ivSize, encryptedBytes, 0, encryptedSize);

        // Hash key.
        byte[] keyBytes = new byte[keySize];
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(sandi.getBytes("UTF-8"));
        System.arraycopy(md.digest(), 0, keyBytes, 0, keyBytes.length);
        SecretKeySpec secretKeySpec = new SecretKeySpec(keyBytes, "AES");

        // Decrypt.
        Cipher cipherDecrypt = Cipher.getInstance("AES/GCM/NOPADDING");
        cipherDecrypt.init(Cipher.DECRYPT_MODE, secretKeySpec, ivParameterSpec);
        byte[] decrypted = cipherDecrypt.doFinal(encryptedBytes);
        return new String(decrypted);
    }

    public static String SHA256 (String text) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");

            md.update(text.getBytes());
            byte[] digest = md.digest();

            return Base64.encodeToString(digest, Base64.DEFAULT).trim();
        }catch (Exception e){
            return null;
        }
    }


    public static Bitmap drawTextToBitmap(Context context, Bitmap originalBitmap, String text) {
        try {
            int extraHeight = (int) (originalBitmap.getHeight() / 10);

            Bitmap newBitmap = Bitmap.createBitmap(originalBitmap.getWidth(),
                    originalBitmap.getHeight() + extraHeight, Bitmap.Config.ARGB_8888);

            Canvas canvas = new Canvas(newBitmap);
            canvas.drawColor(Color.parseColor("#2B2B2B"));
            canvas.drawBitmap(originalBitmap, 0, 0, null);

            Resources resources = context.getResources();
            float scale = resources.getDisplayMetrics().density;

            Paint pText = new Paint();
            pText.setColor(Color.parseColor("#FAFAFB"));

            setTextSizeForWidth(pText,extraHeight-20,text);//(int) (originalBitmap.getHeight() * 0.10),text);


            Rect bounds = new Rect();
            pText.getTextBounds(text, 0, text.length(), bounds);

            Rect textHeightWidth = new Rect();
            pText.getTextBounds(text, 0, text.length(), textHeightWidth);

            canvas.drawText(text, (canvas.getWidth() / 2) - (textHeightWidth.width() / 2),
                    originalBitmap.getHeight() + (extraHeight / 2) + (textHeightWidth.height() / 2),
                    pText);

            return newBitmap;
        } catch (Exception e) {
            return originalBitmap;
        }

    }

    public static void setTextSizeForWidth(Paint paint, float desiredHeight,
                                     String text) {

        // Pick a reasonably large value for the test. Larger values produce
        // more accurate results, but may cause problems with hardware
        // acceleration. But there are workarounds for that, too; refer to
        // http://stackoverflow.com/questions/6253528/font-size-too-large-to-fit-in-cache
        final float testTextSize = 48f;

        // Get the bounds of the text, using our testTextSize.
        paint.setTextSize(testTextSize);
        Rect bounds = new Rect();
        paint.getTextBounds(text, 0, text.length(), bounds);

        // Calculate the desired size as a proportion of our testTextSize.
        float desiredTextSize = testTextSize * desiredHeight / bounds.height();

        // Set the paint for that size.
        paint.setTextSize(desiredTextSize);
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), 0);
    }

    public static void setText(TextView textView, String text, String currency){
        String[] temp;
        String koma = ",";
        try {
            if (text.lastIndexOf(".") > text.lastIndexOf(",")) {
                // jika titik diakhir
                temp = text.split(".");
                koma = ".";
            } else {
                //jika koma diakhir
                temp = text.split(",");
            }
            if (temp[0].length() > 1) {
                text = currency + temp[0] + koma + "<font color='#d5d5d5'>" + temp[1] + "</font>";
                textView.setText(Html.fromHtml(text));
                Utils.log(text);
            } else textView.setText(currency + text);
        }catch (Exception e){
            textView.setText(currency + text);
        }
    }

    public static String toBulan(String bln){
        switch (bln){
            case "01" : return "Jan";
            case "02" : return "Peb";
            case "03" : return "Mar";
            case "04" : return "Apr";
            case "05" : return "Mei";
            case "06" : return "Jun";
            case "07" : return "Jul";
            case "08" : return "Agt";
            case "09" : return "Sep";
            case "10" : return "Okt";
            case "11" : return "Nov";
            case "12" : return "Des";
        }
        return bln;
    }

}
