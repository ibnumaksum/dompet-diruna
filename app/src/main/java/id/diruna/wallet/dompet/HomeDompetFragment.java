package id.diruna.wallet.dompet;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.koushikdutta.async.future.FutureCallback;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import id.diruna.wallet.MainActivity;
import id.diruna.wallet.R;
import id.diruna.wallet.fungsi.StellarAPI;
import id.diruna.wallet.fungsi.Utils;
import id.diruna.wallet.objek.Dompet;
import id.diruna.wallet.objek.DompetCallback;
import id.diruna.wallet.objek.Pemilik;
import io.realm.Realm;
import io.realm.RealmList;

import static android.app.Activity.RESULT_OK;

public class HomeDompetFragment extends Fragment implements DompetCallback {
    StellarAPI stellarAPI;

    TextView txtDRAIDR, txtDRAUSD, txtXLMIDR, txtXLMUSD;

    LineChart chartView;
    Realm realm;
    SwipeRefreshLayout swipe;
    RecyclerView recyclerView;
    int posUpdate = 0, jumlahDompet = 0;
    boolean perbaruiDompetAktif = false;
    AdapterData dompetAdapter;
    Button buttonKirim;

    DecimalFormat precision = new DecimalFormat("###,###.#######");
    public static Float rupiah = 0.0f;
    View view;

    public HomeDompetFragment() {
        // Required empty public constructor
    }

    public static HomeDompetFragment newInstance() {
        return new HomeDompetFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view == null)
            view = inflater.inflate(R.layout.fragment_home_dompet, container, false);
        stellarAPI = new StellarAPI(getContext());
        txtDRAIDR = view.findViewById(R.id.txtDRAIDR);
        txtDRAUSD = view.findViewById(R.id.txtDRAUSD);
        txtXLMIDR = view.findViewById(R.id.txtXLMIDR);
        txtXLMUSD = view.findViewById(R.id.txtXLMUSD);
        chartView = view.findViewById(R.id.chartView);
        buttonKirim = view.findViewById(R.id.buttonKirim);
        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        recyclerView.setItemViewCacheSize(20);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        recyclerView.setNestedScrollingEnabled(false);

        Description description = new Description();
        description.setText("");
        description.setTextAlign(Paint.Align.RIGHT);
        chartView.setDescription(description);
        chartView.getLegend().setTextColor(Color.parseColor("#FAFAFB"));
        chartView.getAxisLeft().setTextColor(Color.parseColor("#FAFAFB"));
        chartView.getAxisRight().setDrawLabels(false);
        chartView.setPinchZoom(false);
        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), TambahDompetPribadiActivity.class);
                intent.putExtra("idPemilik", 1);
                startActivityForResult(intent, 23);
            }
        });

        realm = Realm.getDefaultInstance();

        swipe = view.findViewById(R.id.swipe);

        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getKurs();
                getDataCart();
                getDompetList();
            }
        });

        swipe.post(new Runnable() {
            @Override
            public void run() {
                getKurs();
                getDataCart();
                getDompetList();
            }
        });

        buttonKirim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), KirimTokenActivity.class);
                startActivity(intent);
            }
        });
        return view;
    }


    public void getDompetList() {
        swipe.setRefreshing(true);
        dompetAdapter = new AdapterData(realm.where(Pemilik.class).equalTo("idPemilik", 1).findFirst().dompets, getContext());
        recyclerView.setAdapter(dompetAdapter);

        posUpdate = 0;
        jumlahDompet = dompetAdapter.getItemCount();
        perbaruiDompet();
    }

    public void perbaruiDompet() {
        swipe.setRefreshing(true);
        // update saldo dompet secara bergilir
        if (posUpdate < jumlahDompet && !perbaruiDompetAktif) {
            perbaruiDompetAktif = true;
            Dompet dompet = dompetAdapter.dompets.get(posUpdate);

            Utils.log("onDompetUpdated " + posUpdate);
            stellarAPI.perbaruiSaldoDompet(dompet, this);
        }else
            swipe.setRefreshing(false);
    }

    @Override
    public void onDompetUpdated(Dompet dompet) {
        Utils.log("onDompetUpdated " + posUpdate);
        if (isVisible()) {
            if (!realm.isInTransaction())
                realm.beginTransaction();
            dompetAdapter.dompets.set(posUpdate, dompet);
            realm.commitTransaction();
            dompetAdapter.notifyDataSetChanged();

            posUpdate++;
            perbaruiDompet();
        } else {
            Utils.log("Frgament tidak visible, tidak diupdate");
        }
        perbaruiDompetAktif = false;
        swipe.setRefreshing(false);
    }

    @Override
    public void onResume() {
        super.onResume();
        getDompetList();
    }

    public void getDataCart() {
        swipe.setRefreshing(true);
        stellarAPI.getHourly(new FutureCallback<JsonArray>() {
            @Override
            public void onCompleted(Exception e, JsonArray result) {
                populateChart(result);
            }
        });
    }

    public void populateChart(JsonArray result) {
        List<Entry> entriesxlm = new ArrayList<Entry>();
        List<Entry> entriesdra = new ArrayList<Entry>();
        int jml = result.size();
        for (int n = 0; n < jml; n++) {
            //String tanggal = result.get(n).getAsJsonObject().get("ymdh").getAsString();
            //float jam = Float.parseFloat(tanggal.substring(8));
            float dra = result.get(n).getAsJsonObject().get("dra").getAsFloat();
            float xlm = result.get(n).getAsJsonObject().get("xlm").getAsFloat();
            entriesdra.add(new Entry(n, dra));
            entriesxlm.add(new Entry(n, xlm));
        }

        LineDataSet dra = new LineDataSet(entriesdra, "1 DRA ke Rupiah dalam 24jam terakhir");
        dra.setCircleColor(Color.WHITE);
        dra.setColor(Color.parseColor("#FAFAFB"));
        dra.setDrawValues(false);
        LineDataSet xlm = new LineDataSet(entriesxlm, "XLM");
        xlm.setDrawValues(false);
        List<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        dataSets.add(dra);
        dataSets.add(xlm);
        LineData data = new LineData(dra);
        chartView.setData(data);
        chartView.invalidate();
        swipe.setRefreshing(false);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        recyclerView.setAdapter(null);
        realm.close();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == 23) {
                getDompetList();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void getKurs() {
        swipe.setRefreshing(true);
        SharedPreferences pref = Utils.getPrefKurs(getContext());
        try {
            rupiah = Float.parseFloat(pref.getString("DRAIDR", "0"));
        } catch (Exception e) {
            rupiah = 0f;
        }
        Utils.setText(txtDRAIDR, precision.format(rupiah), "Rp. ");
        try {
            Utils.setText(txtDRAUSD, precision.format(Float.parseFloat(pref.getString("DRAUSD", "0"))), "$ ");
            Utils.setText(txtXLMIDR, precision.format(Float.parseFloat(pref.getString("XLMIDR", "0"))), "Rp. ");
            Utils.setText(txtXLMUSD, precision.format(Float.parseFloat(pref.getString("XLMUSD", "0"))), "$ ");
        } catch (Exception e) {
        }

        stellarAPI.getKurs("DRAIDR", new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                try {
                    Utils.setText(txtDRAIDR, precision.format(Float.parseFloat(result)), "Rp. ");
                    rupiah = Float.parseFloat(result);
                    if (recyclerView.getAdapter() != null) {
                        recyclerView.getAdapter().notifyDataSetChanged();
                    }
                } catch (Exception ee) {
                    //txtDRAIDR.setText("Rp. ~");
                }
                swipe.setRefreshing(true);
                stellarAPI.getKurs("DRAUSD", new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        try {
                            Utils.setText(txtDRAUSD, precision.format(Float.parseFloat(result)), "$ ");
                        } catch (Exception ee) {
                            //txtDRAUSD.setText("$ ~");
                        }
                        swipe.setRefreshing(true);
                        stellarAPI.getKurs("XLMIDR", new FutureCallback<String>() {
                            @Override
                            public void onCompleted(Exception e, String result) {
                                try {
                                    Utils.setText(txtXLMIDR, precision.format(Float.parseFloat(result)), "Rp. ");
                                } catch (Exception ee) {
                                    //txtXLMIDR.setText("Rp. ~");
                                }
                                swipe.setRefreshing(true);
                                stellarAPI.getKurs("XLMUSD", new FutureCallback<String>() {
                                    @Override
                                    public void onCompleted(Exception e, String result) {
                                        try {
                                            Utils.setText(txtXLMUSD, precision.format(Float.parseFloat(result)), "$. ");
                                        } catch (Exception ee) {
                                            //txtXLMUSD.setText("$ ~");
                                        }
                                        swipe.setRefreshing(false);
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }


    public class AdapterData extends RecyclerView.Adapter<AdapterData.ViewHolder> {

        private RealmList<Dompet> dompets;
        int jml;

        Context context;

        public class ViewHolder extends RecyclerView.ViewHolder {
            public TextView namaDompet, saldoDompet;
            public RecyclerView listKoin;
            public LinearLayout layoutDompet;
            public CardView cardView;

            public ViewHolder(View view) {
                super(view);
                namaDompet = view.findViewById(R.id.namaDompet);
                saldoDompet = view.findViewById(R.id.saldoDompet);
                layoutDompet = view.findViewById(R.id.layoutDompet);
                listKoin = view.findViewById(R.id.listKoin);
                cardView = view.findViewById(R.id.cardView);
            }
        }

        public AdapterData(RealmList<Dompet> dompets, Context context) {
            super();
            this.dompets = dompets;
            this.jml = dompets.size();
            this.context = context;
        }


        @Override
        public AdapterData.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_dompet, parent, false);
            return new ViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final AdapterData.ViewHolder holder, final int position) {
            final Dompet dompet = dompets.get(position);
            holder.namaDompet.setText(dompet.namaDompet);
            holder.cardView.setCardBackgroundColor(Color.parseColor(dompet.warnaDompet));
            if (dompet.warnaTeks != null && dompet.warnaTeks.length() > 0)
                holder.saldoDompet.setTextColor(Color.parseColor(dompet.warnaTeks));
            if (dompet.saldos == null || (dompet.saldos != null && dompet.saldos.length() < 10)) {
                holder.saldoDompet.setText("Belum Aktif, aktifkan dengan mengisi saldo 2XLM");
            } else {
                JsonArray json = new JsonParser().parse(dompet.saldos).getAsJsonArray();
                if (json.size() < 3)
                    holder.listKoin.setLayoutManager(new GridLayoutManager(getContext(), json.size()));
                else
                    holder.listKoin.setLayoutManager(new GridLayoutManager(getContext(), 3));
                holder.listKoin.setAdapter(new AdapterKoin(json, dompet, holder.saldoDompet, context));
                holder.layoutDompet.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ((MainActivity) getActivity()).pindahFragment(LihatDompetFragment.newInstance(dompet.publikKey));
                    }
                });
                holder.listKoin.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ((MainActivity) getActivity()).pindahFragment(LihatDompetFragment.newInstance(dompet.publikKey));
                    }
                });
            }
        }

        @Override
        public int getItemCount() {
            return jml;
        }

        public class AdapterKoin extends RecyclerView.Adapter<AdapterKoin.ViewHolder> {

            private JsonArray koins;
            private TextView saldoDompet;
            Context context;
            private Dompet dompet;

            public class ViewHolder extends RecyclerView.ViewHolder {
                public TextView namaKoin, jumlahKoin;
                public ImageView gambarKoin;

                public ViewHolder(View view) {
                    super(view);
                    namaKoin = view.findViewById(R.id.namaKoin);
                    jumlahKoin = view.findViewById(R.id.jumlahKoin);
                    gambarKoin = view.findViewById(R.id.gambarKoin);
                }
            }

            public AdapterKoin(JsonArray koins, Dompet dompet, TextView saldoDompet, Context context) {
                super();
                this.koins = koins;
                this.context = context;
                this.saldoDompet = saldoDompet;
                this.dompet = dompet;
                Utils.log(koins.size() + " koins");
            }

            @Override
            public AdapterKoin.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_coin, parent, false);
                return new ViewHolder(itemView);
            }

            @Override
            public void onBindViewHolder(final AdapterKoin.ViewHolder holder, final int position) {
                final JsonObject koin = koins.get(position).getAsJsonObject();
                String namaKoin = "";
                if (koin.get("asset_type").getAsString().equals("native")) {
                    namaKoin = "XLM";
                } else {
                    namaKoin = koin.get("asset_code").getAsString();
                }

                holder.namaKoin.setText(namaKoin);

                if (dompet.warnaTeks != null && dompet.warnaTeks.length() > 0) {
                    holder.namaKoin.setTextColor(Color.parseColor(dompet.warnaTeks));
                    //holder.jumlahKoin.setText(precision.format(koin.get("balance").getAsFloat()));
                    holder.jumlahKoin.setTextColor(Color.parseColor(dompet.warnaTeks));
                }
                Utils.setText(holder.jumlahKoin, precision.format(koin.get("balance").getAsFloat()), "");
                Utils.log(namaKoin);
                if (namaKoin.equals("DRA")) {
                    holder.gambarKoin.setImageResource(R.drawable.ic_diruna);
                    Utils.setText(saldoDompet, precision.format(koin.get("balance").getAsFloat() * rupiah), "± Rp. ");
                } else {
                    String huruf = namaKoin.toLowerCase().substring(0, 1);
                    int id = context.getResources().getIdentifier("ic_huruf_" + huruf, "drawable", context.getPackageName());
                    holder.gambarKoin.setImageResource(id);
                }
            }

            @Override
            public int getItemCount() {
                return koins.size();
            }
        }
    }


}
