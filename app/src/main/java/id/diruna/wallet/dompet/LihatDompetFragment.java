package id.diruna.wallet.dompet;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.koushikdutta.async.Util;
import com.koushikdutta.async.future.FutureCallback;

import java.text.DecimalFormat;
import java.util.TimerTask;

import id.diruna.wallet.MainActivity;
import id.diruna.wallet.R;
import id.diruna.wallet.barcode.BarcodeGeneratorActivity;
import id.diruna.wallet.fungsi.StellarAPI;
import id.diruna.wallet.fungsi.Utils;
import id.diruna.wallet.objek.Dompet;
import id.diruna.wallet.objek.Transaksi;
import io.realm.OrderedCollectionChangeSet;
import io.realm.OrderedRealmCollection;
import io.realm.OrderedRealmCollectionChangeListener;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmList;
import io.realm.RealmRecyclerViewAdapter;
import io.realm.RealmResults;
import io.realm.Sort;


public class LihatDompetFragment extends Fragment implements View.OnClickListener, RealmChangeListener {
    private static final String ARG_PARAM1 = "param1";
    public TextView namaDompet, saldoDompet ;
    public RecyclerView listKoin;
    public LinearLayout layoutDompet;
    public CardView cardView;
    View view;
    Button buttonLihatQR,buttonKirim;
    Realm realmup;
    Dompet dompet;
    RecyclerView listTransaksi;
    SwipeRefreshLayout swipe;
    DecimalFormat precision = new DecimalFormat("###,###.#######");
    AdapterData adapterData;
    StellarAPI stellarAPI;
    private String idDommpet;


    public LihatDompetFragment() {
        // Required empty public constructor
    }


    public static LihatDompetFragment newInstance(String idDommpet) {
        LihatDompetFragment fragment = new LihatDompetFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, idDommpet);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            idDommpet = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if(view==null)
            view = inflater.inflate(R.layout.fragment_lihat_dompet, container, false);
        buttonLihatQR = view.findViewById(R.id.buttonLihatQR);
        buttonLihatQR.setOnClickListener(this);
        buttonKirim = view.findViewById(R.id.buttonKirim);
        buttonKirim.setOnClickListener(this);

        realmup = Realm.getDefaultInstance();
        stellarAPI = new StellarAPI(getContext());

        namaDompet  = view.findViewById(R.id.namaDompet);
        saldoDompet = view.findViewById(R.id.saldoDompet);
        layoutDompet = view.findViewById(R.id.layoutDompet);
        listKoin    = view.findViewById(R.id.listKoin);
        cardView = view.findViewById(R.id.cardView);
        listTransaksi = view.findViewById(R.id.listTransaksi);
        listTransaksi.setHasFixedSize(true);
        listTransaksi.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        listTransaksi.setItemViewCacheSize(20);
        listTransaksi.setDrawingCacheEnabled(true);
        listTransaksi.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        listTransaksi.setNestedScrollingEnabled(false);

        swipe = view.findViewById(R.id.swipe);

        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(dompet==null) return;
                //showData();
                getOperations();
            }
        });
        swipe.post(new Runnable() {
            @Override
            public void run() {
                swipe.setRefreshing(true);
                realmup.where(Dompet.class).equalTo("publikKey",idDommpet).findFirstAsync().addChangeListener(LihatDompetFragment.this);
            }
        });

        return view;
    }

    @Override
    public void onChange(Object o) {
        Utils.log("onchange");
        if(o != null && o instanceof Dompet){
            dompet = (Dompet)o;
            if(dompet.isLoaded()) {
                namaDompet.setText(dompet.namaDompet);
                cardView.setCardBackgroundColor(Color.parseColor(dompet.warnaDompet));
                if (dompet.warnaTeks != null && dompet.warnaTeks.length() > 0)
                    saldoDompet.setTextColor(Color.parseColor(dompet.warnaTeks));
                if (dompet.saldos == null || (dompet.saldos != null && dompet.saldos.length() < 10)) {
                    saldoDompet.setText("Belum Aktif, aktifkan dengan mengisi saldo 2XLM");
                } else {
                    JsonArray json = new JsonParser().parse(dompet.saldos).getAsJsonArray();
                    if (json.size() < 3)
                        listKoin.setLayoutManager(new GridLayoutManager(getContext(), json.size()));
                    else
                        listKoin.setLayoutManager(new GridLayoutManager(getContext(), 3));
                    listKoin.setAdapter(new AdapterKoin(json, dompet, saldoDompet, getContext()));
                }
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if(adapterData==null){
                            adapterData = new AdapterData(realmup.where(Transaksi.class)
                                    .equalTo("publicKey",dompet.publikKey)
                                    .sort("paging_tokenO",Sort.DESCENDING)
                                    .findAllAsync()
                                    ,dompet.publikKey,getContext());
                        }
                        if(listTransaksi.getAdapter()==null){
                            listTransaksi.setAdapter(adapterData);
                        }
                        getOperations();
                    }
                },500);
            }
        }
    }


    public void getOperations(){
        new DownloadOperation().execute(dompet.publikKey);
    }

    private class DownloadOperation extends AsyncTask<String, Void, Integer> {

        @Override
        protected void onPreExecute() {
            swipe.setRefreshing(true);
        }

        protected Integer doInBackground(String... strings) {
            String publikKey = strings[0];
            // Open the Realm
            Realm realm = Realm.getDefaultInstance();
            int jml = 0;
            try {
                String paging_tokenO = ""+realm.where(Transaksi.class).equalTo("publicKey", publikKey).max("paging_tokenO");
                if(paging_tokenO.length()<5){
                    paging_tokenO = "";
                }
                Utils.log("paging_tokenO: "+paging_tokenO);
                JsonObject result = stellarAPI.getOperations(publikKey, paging_tokenO, 10);
                if(result==null){
                    Utils.log("Hasil Null");
                }else {
                    if (result.has("status") && !result.get("status").isJsonNull()) {
                        //Utils.showDialog(result.get("title").getAsString(), result.get("detail").getAsString(), getContext());
                    } else {
                        //try{
                        JsonArray records = result.get("_embedded").getAsJsonObject().get("records").getAsJsonArray();
                        jml = records.size();
                        for (int n = 0; n < jml; n++) {
                            JsonObject rx = records.get(n).getAsJsonObject();
                            Utils.log(rx.toString());
                            if (rx.get("type").getAsString().equals("payment")) {
                                if (!realm.isInTransaction()) realm.beginTransaction();
                                Transaksi tx = new Transaksi(rx.get("transaction_hash").getAsString());
                                tx.publicKey = publikKey;
                                tx.created_at = rx.get("created_at").getAsString();
                                tx.asset_type = rx.get("asset_type").getAsString();
                                if (!tx.asset_type.equals("native"))
                                    tx.asset_code = rx.get("asset_code").getAsString();
                                else
                                    tx.asset_code = "XLM";
                                tx.from = rx.get("from").getAsString();
                                tx.to = rx.get("to").getAsString();
                                tx.amount = rx.get("amount").getAsFloat();
                                tx.paging_tokenO = Long.parseLong(rx.get("paging_token").getAsString());
                                JsonObject hasil = stellarAPI.getTransactions(publikKey, (tx.paging_tokenO-2), 1);
                                try{
                                    JsonObject rtx = hasil.get("_embedded").getAsJsonObject().get("records").getAsJsonArray().get(0).getAsJsonObject();
                                    if(tx.paging_tokenO-1==rtx.get("paging_token").getAsLong()) {
                                        if (rtx.has("memo"))
                                            tx.memo = rtx.get("memo").getAsString();
                                        else
                                            tx.memo = " ";
                                        tx.paging_tokenT = rtx.get("paging_token").getAsLong();
                                    }else{
                                        Utils.log("paging_tokenT beda: "+tx.paging_tokenO+"--"+rtx.get("paging_token").getAsLong());
                                    }
                                }catch (Exception ee){}

                                realm.copyToRealmOrUpdate(tx);
                                realm.commitTransaction();
                                publishProgress();
                            } else {
                                Utils.log("Not Payment");
                            }
                        }
                    }
                }

            } finally {
                realm.close();
            }
            return jml;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            if(listTransaksi!=null && listTransaksi.getAdapter()!=null && isVisible())
                listTransaksi.getAdapter().notifyDataSetChanged();
        }

        protected void onPostExecute(Integer jumlah) {
            swipe.setRefreshing(false);
            if(isVisible())
                if(jumlah>0)
                    getOperations();
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        realmup.close();
        listTransaksi = null;
    }

    @Override
    public void onClick(View v) {
        if(v==buttonLihatQR) {
            Intent intent = new Intent(getContext(), BarcodeGeneratorActivity.class);
            intent.putExtra("isiQRCode", "DRASEND://" + dompet.publikKey);
            intent.putExtra("judulQRCode", dompet.namaDompet);
            startActivity(intent);
        }else if(buttonKirim==v){
            Intent intent = new Intent(getContext(), KirimTokenActivity.class);
            intent.putExtra("publikKey",dompet.publikKey);
            startActivity(intent);
        }
    }

    public class AdapterData extends RealmRecyclerViewAdapter<Transaksi, AdapterData.ViewHolder> {

        Context context;
        private String publicKey;

        public AdapterData(OrderedRealmCollection<Transaksi> transaksis, String publicKey, Context context) {
            super(transaksis,true);
            this.context = context;
            this.publicKey = publicKey;
        }

        @Override
        public AdapterData.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_transaksi, parent, false);
            return new AdapterData.ViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final AdapterData.ViewHolder holder, final int position) {
            final Transaksi transaksi = getItem(position);
            if(transaksi.from.equals(publicKey)){
                holder.txStatus.setBackgroundResource(R.color.colorKirim);
                holder.txtPublicKey.setText(transaksi.to);
            }else {
                holder.txStatus.setBackgroundResource(R.color.colorTerima);
                holder.txtPublicKey.setText(transaksi.from);
            }
            holder.txtPublicKey.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((MainActivity)getActivity()).bukaWeb("http://"+holder.txtPublicKey.getText().toString()+".diruna.id");
                }
            });
            holder.txtNamaToken.setText(transaksi.asset_code);
            Utils.setText(holder.txtJumlah,precision.format(transaksi.amount),"");

            //"2018-09-06T04:53:27Z"
            //Utils.log(transaksi.created_at);
            if(transaksi.created_at!=null && transaksi.created_at.length()>10) {
                holder.txtTgl.setText(transaksi.created_at.substring(8, 10));
                String[] tmp = transaksi.created_at.substring(0, 7).split("-");
                holder.txtBlnThn.setText(Utils.toBulan(tmp[1])+" "+tmp[0].substring(2));
                holder.txtJam.setText(transaksi.created_at.substring(11, 16));
            }
            if (transaksi.memo != null) {
                try {
                    holder.txtMemo.setText(transaksi.memo);
                } catch (Exception e) {
                }
            }

        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public TextView txtTgl, txtBlnThn, txtJam, txtNamaToken, txtJumlah, txtPublicKey,txtMemo ;
            public LinearLayout txStatus;
            public ViewHolder(View view) {
                super(view);
                txStatus  = view.findViewById(R.id.txStatus);
                txtTgl = view.findViewById(R.id.txtTgl);
                txtBlnThn    = view.findViewById(R.id.txtBlnThn);
                txtJam    = view.findViewById(R.id.txtJam);
                txtNamaToken    = view.findViewById(R.id.txtNamaToken);
                txtJumlah    = view.findViewById(R.id.txtJumlah);
                txtPublicKey    = view.findViewById(R.id.txtPublicKey);
                txtMemo    = view.findViewById(R.id.txtMemo);
            }
        }

    }

    public class AdapterKoin extends RecyclerView.Adapter<AdapterKoin.ViewHolder> {

        Context context;
        private JsonArray koins;
        private TextView saldoDompet;
        private Dompet dompet;

        public AdapterKoin(JsonArray koins,Dompet dompet,TextView saldoDompet, Context context) {
            super();
            this.koins = koins;
            this.context = context;
            this.saldoDompet = saldoDompet;
            this.dompet = dompet;
            Utils.log(koins.size()+" koins");
        }

        @Override
        public AdapterKoin.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_coin, parent, false);
            return new AdapterKoin.ViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final AdapterKoin.ViewHolder holder, final int position) {
            final JsonObject koin = koins.get(position).getAsJsonObject();
            String namaKoin = "";
            if(koin.get("asset_type").getAsString().equals("native")){
                namaKoin = "XLM";
            }else{
                namaKoin = koin.get("asset_code").getAsString();
            }

            holder.namaKoin.setText(namaKoin);

            if(dompet.warnaTeks!=null && dompet.warnaTeks.length()>0) {
                holder.namaKoin.setTextColor(Color.parseColor(dompet.warnaTeks));
                //holder.jumlahKoin.setText(precision.format(koin.get("balance").getAsFloat()));
                holder.jumlahKoin.setTextColor(Color.parseColor(dompet.warnaTeks));
            }
            Utils.setText(holder.jumlahKoin,precision.format(koin.get("balance").getAsFloat()),"");
            Utils.log(namaKoin);
            if(namaKoin.equals("DRA")){
                holder.gambarKoin.setImageResource(R.drawable.ic_diruna);
                Utils.setText(saldoDompet,precision.format(koin.get("balance").getAsFloat()*HomeDompetFragment.rupiah),"± Rp. ");
            }else{
                String huruf = namaKoin.toLowerCase().substring(0,1);
                int id = context.getResources().getIdentifier("ic_huruf_"+huruf, "drawable", context.getPackageName());
                holder.gambarKoin.setImageResource(id);
            }
        }

        @Override
        public int getItemCount() {
            return koins.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public TextView namaKoin, jumlahKoin ;
            public ImageView gambarKoin;
            public ViewHolder(View view) {
                super(view);
                namaKoin  = view.findViewById(R.id.namaKoin);
                jumlahKoin = view.findViewById(R.id.jumlahKoin);
                gambarKoin    = view.findViewById(R.id.gambarKoin);
            }
        }
    }
}
