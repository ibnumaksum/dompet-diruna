package id.diruna.wallet.dompet;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.koushikdutta.async.future.FutureCallback;

import org.stellar.sdk.KeyPair;

import id.diruna.wallet.R;
import id.diruna.wallet.fungsi.StellarAPI;
import id.diruna.wallet.fungsi.Utils;
import id.diruna.wallet.objek.Dompet;
import id.diruna.wallet.objek.Pemilik;
import id.diruna.wallet.picker.PinRequestActivity;
import io.realm.Realm;

public class TambahDompetPribadiActivity extends AppCompatActivity implements View.OnClickListener,FutureCallback<String> ,RadioGroup.OnCheckedChangeListener {

    EditText inputNamaDompet,inputPrivateKey;
    ProgressBar progressBar;
    TextView txtPenjelasan,txtNamaPemilik,txtJudulTambah;
    CardView cardView;
    Button tombolBaru,tombolTambah;
    RadioGroup radioWarna,radioWarnaText;

    Realm realm;
    private Pemilik pemilik;
    Dompet dompet;
    String hexColor = "#CCFFFFFF",hexTextColor = "#000000";

    protected String pin = "", publicKey = "";
    protected boolean isDompetBaru = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_dompet_pribadi);

        progressBar = findViewById(R.id.progressBar);
        cardView = findViewById(R.id.layoutAdd);
        txtPenjelasan = findViewById(R.id.txtPenjelasan);
        inputNamaDompet = findViewById(R.id.inputNamaDompet);
        inputPrivateKey = findViewById(R.id.inputPrivateKey);
        txtNamaPemilik = findViewById(R.id.txtNamaPemilik);
        tombolBaru = findViewById(R.id.tombolBaru);
        tombolTambah = findViewById(R.id.tombolTambah);
        radioWarna = findViewById(R.id.radioWarna);
        radioWarnaText = findViewById(R.id.radioWarnaText);
        txtJudulTambah = findViewById(R.id.txtJudulTambah);

        radioWarna.setOnCheckedChangeListener(this);
        radioWarnaText.setOnCheckedChangeListener(this);

        tombolBaru.setOnClickListener(this);
        tombolTambah.setOnClickListener(this);

        Intent intent = getIntent();
        realm = Realm.getDefaultInstance();

        pemilik = realm.where(Pemilik.class).equalTo("idPemilik",1).findFirst();
        //harus ada pemilik
        if(pemilik==null) finish();
        txtNamaPemilik.setText(pemilik.namaPemilik);
        if(intent.hasExtra("idDompet")){
            dompet = pemilik.dompets.where().equalTo("publikKey",intent.getStringExtra("idDompet")).findFirst(); //.getById(Long.parseLong(intent.getLongExtra("idDompet",1)+"",32));
            inputNamaDompet.setText(dompet.namaDompet);
            txtJudulTambah.setText("Ubah Dompet");
            if(dompet.privateKeyDompet!=null && dompet.privateKeyDompet.length()>5)
                txtPenjelasan.setText("Private Key Hidden karena terenkripsi, isi kembali jika ingin diubah\n"+txtPenjelasan.getText().toString());
        }else if(intent.hasExtra("privateKey")){
            inputPrivateKey.setText(intent.getStringExtra("privateKey"));
        }else if(intent.hasExtra("namaDompet")){
            inputNamaDompet.setText(intent.getStringExtra("namaDompet"));
        }

    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        if(group == radioWarnaText){
            Drawable background = findViewById(checkedId).getBackground();
            if (background instanceof ColorDrawable) {
                int color = ((ColorDrawable) background).getColor();
                hexTextColor = String.format("#%06X", (0xFFFFFF & color));
                Utils.log(hexTextColor);
                txtPenjelasan.setTextColor(Color.parseColor(hexTextColor));
            }
        }else{
            Drawable background = findViewById(checkedId).getBackground();
            if (background instanceof ColorDrawable) {
                int color = ((ColorDrawable) background).getColor();
                hexColor = String.format("#CC%06X", (0xFFFFFF & color));
                Utils.log(hexColor);
                cardView.setCardBackgroundColor(Color.parseColor(hexColor));
            }
        }
    }

    @Override
    public void onClick(View v) {
        if(v==tombolTambah){
            if(dompet!=null && inputPrivateKey.getText().toString().length()<50){
                //Jika hanya update judul dompet
                if(!realm.isInTransaction())
                    realm.beginTransaction();
                dompet = pemilik.dompets.where().equalTo("publikKey",dompet.publikKey).findFirst();
                dompet.namaDompet = inputNamaDompet.getText().toString();
                dompet.warnaDompet = hexColor;
                dompet.warnaTeks = hexTextColor;
                if(dompet.namaDompet.length()==0){
                    dompet.namaDompet = dompet.publikKey;
                }
                realm.commitTransaction();

                setResult(RESULT_OK);
                finish();
            }else
            startActivityForResult(new Intent(this,PinRequestActivity.class),33);
        }else if(v==tombolBaru){
            new AlertDialog.Builder(this)
                    .setIcon(R.mipmap.ic_launcher)
                    .setTitle(R.string.app_name)
                    .setMessage("Yakin mau membuat Dompet Baru")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            isDompetBaru = true;
                            KeyPair keyPair = KeyPair.random();
                            inputPrivateKey.setText(new String(keyPair.getSecretSeed()));
                            publicKey = keyPair.getAccountId();
                            Utils.showToast("Dompet Baru telah dibuat, tapi belum Aktif, silahkan isi 2 XLM untuk mengaktifkannya, jangan lupa backup Dompet anda",TambahDompetPribadiActivity.this);
                        }
                    })
                    .setNegativeButton("Batal",null)
                    .show();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode==RESULT_OK && requestCode==33){
            this.pin = data.getStringExtra("pin");
            cardView.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);

            if(dompet==null) {
                if (isDompetBaru) {
                    if(!realm.isInTransaction())
                        realm.beginTransaction();
                    pemilik = realm.where(Pemilik.class).equalTo("idPemilik",1).findFirst();
                    dompet = realm.createObject(Dompet.class,publicKey);
                    dompet.namaDompet = inputNamaDompet.getText().toString();
                    dompet.warnaDompet = hexColor;
                    dompet.warnaTeks = hexTextColor;
                    try {
                        dompet.privateKeyDompet = Utils.enkripsi(inputPrivateKey.getText().toString(), pin);
                    }catch (Exception ee){
                        realm.cancelTransaction();
                        Utils.showDialog("Gagal menyimpan dompet, gagal enkripsi",this);
                        cardView.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);
                        return;
                    }
                    pemilik.dompets.add(dompet);
                    realm.commitTransaction();
                    setResult(RESULT_OK);
                    finish();
                }
            }
            try {
                KeyPair keypair = KeyPair.fromSecretSeed(inputPrivateKey.getText().toString().trim());
                publicKey = keypair.getAccountId();
                new StellarAPI(this).ambilDataDompet(publicKey.trim(), this);
            }catch (Exception e){
                Utils.showDialog("PrivateKey salah\n\n"+e.getMessage(),this);
            }
        }
    }

    @Override
    public void onCompleted(Exception e, String hasil) {
        Utils.log(hasil);
        if(e!=null && e.getMessage()!=null && e.getMessage().length()>0){
            Utils.showDialog(e.getMessage(),this);
            return;
        }else{
            if(!realm.isInTransaction())
                realm.beginTransaction();

            String namaDompet = inputNamaDompet.getText().toString().trim();
            if(namaDompet.length()<2){
                namaDompet = publicKey.trim().toUpperCase();
            }
            try {
                pemilik = realm.where(Pemilik.class).equalTo("idPemilik",1).findFirst();
                if(dompet==null)
                    dompet = realm.createObject(Dompet.class,publicKey);
                else
                    dompet = pemilik.dompets.where().equalTo("publikKey",dompet.publikKey).findFirst();
                dompet.namaDompet = namaDompet;
                dompet.warnaDompet = hexColor;
                dompet.warnaTeks = hexTextColor;
                try {
                    dompet.privateKeyDompet = Utils.enkripsi(inputPrivateKey.getText().toString(), pin);
                }catch (Exception ee){
                    realm.cancelTransaction();
                    Utils.showDialog("Gagal menyimpan dompet, gagal enkripsi",this);
                    cardView.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);
                    return;
                }

                JsonObject result = new JsonParser().parse(hasil).getAsJsonObject();
                if(!result.has("status") || (result.has("status") && result.get("status").getAsInt()!=404)) {
                    Utils.log(result.toString());
                    if (result.has("home_domain"))
                        dompet.home_domain = result.get("home_domain").getAsString();
                    if (result.has("sequence"))
                        dompet.sequence = result.get("sequence").getAsString();
                    if (result.has("subentry_count"))
                        dompet.subentry_count = result.get("subentry_count").getAsString();
                    if (result.has("balances"))
                        dompet.saldos = result.get("balances").getAsJsonArray().toString();
                }

                pemilik.dompets.add(dompet);
                realm.commitTransaction();

                cardView.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
                setResult(RESULT_OK);
                finish();
            }catch (Exception ee){
                realm.cancelTransaction();
                Utils.log(ee.getMessage());
                if(ee.getMessage().startsWith("Primary key value already exists:")){
                    Utils.showDialog("Dompet sudah ada di database",this);
                }
                inputPrivateKey.setError("PrivateKey tidak valid");
                cardView.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
            }
        }
    }
}
