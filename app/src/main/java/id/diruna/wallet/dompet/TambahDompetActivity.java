package id.diruna.wallet.dompet;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.koushikdutta.async.future.FutureCallback;

import id.diruna.wallet.BaseActivity;
import id.diruna.wallet.R;
import id.diruna.wallet.fungsi.StellarAPI;
import id.diruna.wallet.fungsi.Utils;
import id.diruna.wallet.objek.Dompet;
import id.diruna.wallet.objek.Pemilik;
import io.realm.Realm;

public class TambahDompetActivity extends BaseActivity implements FutureCallback<String>{

    EditText inputNamaDompet,inputPublicKey;
    ProgressBar progressBar;
    TextView txtNamaPemilik;
    CardView cardView;

    Realm realm;
    private Pemilik pemilik;
    long idPemilik = 0;
    Dompet dompet;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_dompet);
        progressBar = findViewById(R.id.progressBar);
        cardView = findViewById(R.id.layoutAdd);
        inputNamaDompet = findViewById(R.id.inputNamaDompet);
        inputPublicKey = findViewById(R.id.inputPublicKey);
        txtNamaPemilik = findViewById(R.id.txtNamaPemilik);

        Intent intent = getIntent();
        if(intent.hasExtra("idPemilik")){
            idPemilik = Long.parseLong(intent.getLongExtra("idPemilik",1)+"",32);
        }

        realm = Realm.getDefaultInstance();

        if(idPemilik==0){
            finish();
        }


        pemilik = realm.where(Pemilik.class).equalTo("idPemilik",idPemilik).findFirst();
        //harus ada pemilik
        if(pemilik==null) finish();

        txtNamaPemilik.setText(pemilik.namaPemilik);
        if(intent.hasExtra("idDompet")){
            dompet = pemilik.dompets.where().equalTo("publikKey",intent.getStringExtra("idDompet")).findFirst(); //.getById(Long.parseLong(intent.getLongExtra("idDompet",1)+"",32));
            inputNamaDompet.setText(dompet.namaDompet);
            inputPublicKey.setText(dompet.publikKey);
        }
    }

    public void tambahkan(View view){

        if(inputPublicKey.getText().length()<2){
            inputPublicKey.setError("Mohon Isi Public Key Dompet");
            return;
        }

        //jika menambahkan baru, maka cek apakah sudah ada atau belum
        if(dompet==null && pemilik.dompets!=null) {
            if(realm.where(Dompet.class).equalTo("publikKey",inputPublicKey.getText().toString().trim()).findFirst()!=null){
                inputPublicKey.setError("PublikKey/Dompet sudah ada");
                return;
            }
        }


        cardView.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        new StellarAPI(this).ambilDataDompet(inputPublicKey.getText().toString().trim(),this);

    }



    @Override
    public void onCompleted(Exception e, String hasil) {
        Utils.log(hasil);
        if(e!=null && e.getMessage().length()>0){
            Utils.showDialog(e.getMessage(),this);
            cardView.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
            return;
        }
        String namaDompet = inputNamaDompet.getText().toString().trim();
        if(namaDompet.length()==0){
            namaDompet = inputPublicKey.getText().toString().trim().toUpperCase();
        }

        try {
            JsonObject result = new JsonParser().parse(hasil).getAsJsonObject();

            boolean isNew = false;
            // Jika buat baru maka null
            if (dompet == null) {
                dompet = new Dompet(namaDompet, inputPublicKey.getText().toString().trim());
                isNew = true;
            }


            dompet.home_domain = result.get("home_domain").getAsString();
            dompet.sequence = result.get("sequence").getAsString();
            dompet.subentry_count = result.get("subentry_count").getAsString();
            dompet.saldos = result.get("balances").getAsJsonArray().getAsString();
            pemilik.dompets.add(dompet);

            cardView.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
            finish();
        }catch (Exception ee){
            inputPublicKey.setError("Public Key tidak valid");
            cardView.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
        }
    }}
