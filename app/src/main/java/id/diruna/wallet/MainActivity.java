package id.diruna.wallet;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.customtabs.CustomTabsIntent;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nabinbhandari.android.permissions.PermissionHandler;
import com.nabinbhandari.android.permissions.Permissions;

import org.stellar.sdk.Server;

import id.diruna.wallet.barcode.BarcodeScannerActivity;
import id.diruna.wallet.biodata.AddEditBiodata;
import id.diruna.wallet.dompet.HomeDompetFragment;
import id.diruna.wallet.dompet.KirimTokenActivity;
import id.diruna.wallet.objek.Pemilik;
import io.realm.Realm;
import saschpe.android.customtabs.CustomTabsHelper;
import saschpe.android.customtabs.WebViewFallback;

public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    Toolbar toolbar;

    Server server = new Server("https://horizon.stellar.org");

    TextView txtNamaLengkap,txtNomorHP,txtEmail;
    ImageView imgAvatar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);



        txtNamaLengkap = navigationView.getHeaderView(0).findViewById(R.id.txtNamaLengkap);
        txtNomorHP = navigationView.getHeaderView(0).findViewById(R.id.txtNomorHP);
        txtEmail = navigationView.getHeaderView(0).findViewById(R.id.txtEmail);
        imgAvatar = navigationView.getHeaderView(0).findViewById(R.id.imgAvatar);

        txtNamaLengkap.setOnClickListener(klikBio);
        txtNomorHP.setOnClickListener(klikBio);
        txtEmail.setOnClickListener(klikBio);
        imgAvatar.setOnClickListener(klikBio);

        getSupportFragmentManager().beginTransaction().add(R.id.container, HomeDompetFragment.newInstance()).commit();

        setBiodata();
    }


    public void setBiodata(){
        Pemilik pemilik = Realm.getDefaultInstance().where(Pemilik.class).equalTo("idPemilik",1).findFirst();
        txtNamaLengkap.setText(pemilik.namaPemilik);
        txtNomorHP.setText(pemilik.nomorHP);
        txtEmail.setText(pemilik.email);
        if(pemilik.foto!=null)
            imgAvatar.setImageBitmap(BitmapFactory.decodeByteArray(pemilik.foto,0,pemilik.foto.length));
    }

    View.OnClickListener klikBio = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(MainActivity.this,AddEditBiodata.class);
            intent.putExtra("idPemilik",1);
            startActivityForResult(intent,22);
        }
    };


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode==RESULT_OK) {
            if (requestCode == 22) {
                setBiodata();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void pindahFragment(Fragment fragment){
        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.slide_left_enter,R.anim.slide_left_exit, R.anim.slide_right_enter,R.anim.slide_right_exit)
                .replace(R.id.container,fragment)
                .addToBackStack(null)
                .commit();
    }

    public void backToHome(){
        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.slide_left_enter,R.anim.slide_left_exit, R.anim.slide_right_enter,R.anim.slide_right_exit)
                .replace(R.id.container,HomeDompetFragment.newInstance()).commit();
    }

    public void kembali(View view){
        getSupportFragmentManager()
                .popBackStack();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_scan:
                Permissions.check(this/*context*/, Manifest.permission.CAMERA, null, new PermissionHandler() {
                    @Override
                    public void onGranted() {
                        startActivity(new Intent(MainActivity.this,BarcodeScannerActivity.class));
                    }
                });
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_dompet) {
            backToHome();
        } else if (id == R.id.nav_buku_alamat) {

        } else if (id == R.id.nav_belanja) {
            bukaWeb("https://dirunapoint.com/");
        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_diruna_org) {
            bukaWeb("https://diruna.org/");
        } else if (id == R.id.nav_diruna_id) {
            bukaWeb("https://diruna.id/");
        }else if(id == R.id.nav_tentang){
            new AlertDialog.Builder(this)
                    .setIcon(R.drawable.ibnux)
                    .setTitle("Ibnu Maksum @iBNuX")
                    .setMessage("Seorang Programmer yang suka iseng bikin aplikasi sampe lupa waktu")
                    .setPositiveButton("DONASI DRA", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(MainActivity.this, KirimTokenActivity.class);
                            intent.putExtra("publicKey", "");
                            intent.putExtra("jumlah", "4");
                            intent.putExtra("memo", "Terima kasih");
                            startActivity(intent);
                        }
                    })
                    .setNegativeButton("Terima kasih", null)
                    .show();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    public void bukaWeb(String urlnya){
        CustomTabsIntent customTabsIntent = new CustomTabsIntent.Builder()
                .addDefaultShareMenuItem()
                .setToolbarColor(this.getResources().getColor(R.color.colorPrimary))
                .setShowTitle(true)
                .build();
        CustomTabsHelper.openCustomTab(this, customTabsIntent,
                Uri.parse(urlnya),
                new WebViewFallback());
    }
}
