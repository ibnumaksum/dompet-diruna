package id.diruna.wallet.barcode;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;

import id.diruna.wallet.dompet.KirimTokenActivity;
import id.diruna.wallet.dompet.TambahDompetPribadiActivity;
import id.diruna.wallet.fungsi.Utils;
import id.diruna.wallet.picker.PinRequestActivity;
import me.dm7.barcodescanner.zbar.Result;
import me.dm7.barcodescanner.zbar.ZBarScannerView;

public class BarcodeScannerActivity extends AppCompatActivity implements ZBarScannerView.ResultHandler {
    private ZBarScannerView mScannerView;

    String namaDompet = "", privateKey = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setTitle("Scan Barcode");
        mScannerView = new ZBarScannerView(this);    // Programmatically initialize the scanner view
        setContentView(mScannerView);                // Set the scanner view as the content view
        //setContentView(R.layout.activity_barcode_scanner);
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera();          // Start camera on resume
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();           // Stop camera on pause
    }

    @Override
    public void handleResult(Result rawResult) {
        // DRASEND://ALAMAT/[RP]JUMLAH/PESAN
        // jika RP230000 maka akan dikonversi ke Nilai Diruna, jika tanpa RP akan langsung jadi DRA
        // DRABAK:PRIVATEKEY/Nama Dompet
        // DRABAKS:ENCRYPTED PRIVATEKEY/Nama Dompet
        String hasil = rawResult.getContents();
        if(hasil.startsWith("DRABAK:")) {
            String temp[] = hasil.split(":");
            Intent intent = new Intent(this, TambahDompetPribadiActivity.class);
            if (temp.length > 0 && temp[1] != null)
                intent.putExtra("privateKey", temp[1]);
            if (temp.length > 2 && temp[2] != null)
                intent.putExtra("namaDompet", temp[2]);
            startActivity(intent);
            finish();
        }else if(hasil.startsWith("DRABAKS:")){
            String temp[] = hasil.split(":");
            if(temp.length > 0 && temp[1]!=null)
                privateKey = temp[1];
            if(temp.length > 1 && temp[2]!=null)
                namaDompet = temp[2];
            startActivityForResult(new Intent(this,PinRequestActivity.class),32);
        }else if(hasil.startsWith("DRASEND://")){
            String temp[] = hasil.split("//");
            if(temp.length==2) {
                temp = temp[1].split("/");
                Intent intent = new Intent(this, KirimTokenActivity.class);
                if (temp[0] != null)
                    intent.putExtra("publicKey", temp[0]);
                if (temp.length > 1 && temp[1] != null)
                    intent.putExtra("jumlah", temp[1]);
                if (temp.length > 2 && temp[2] != null)
                    intent.putExtra("memo", temp[2]);
                startActivity(intent);
            }
            finish();
        }


        // If you would like to resume scanning, call this method below:
        mScannerView.resumeCameraPreview(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode==RESULT_OK){
            String pin = data.getStringExtra("pin");
            Intent intent = new Intent(this, TambahDompetPribadiActivity.class);
            try{
                intent.putExtra("privateKey", Utils.dekripsi(privateKey,pin));
                intent.putExtra("namaDompet", namaDompet);
            }catch (Exception e){
                Utils.showDialog("PIN SALAH",this);
            }
            startActivity(intent);
        }else finish();
    }
}
