package id.diruna.wallet.barcode;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.github.sumimakito.awesomeqr.AwesomeQrRenderer;
import com.github.sumimakito.awesomeqr.RenderResult;
import com.github.sumimakito.awesomeqr.option.RenderOption;
import com.github.sumimakito.awesomeqr.option.color.Color;
import com.github.sumimakito.awesomeqr.option.logo.Logo;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import id.diruna.wallet.R;
import id.diruna.wallet.fungsi.Utils;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;

public class QRcodeGeneratorFragment extends Fragment implements RadioGroup.OnCheckedChangeListener {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String isiQRCode;
    ImageView imageQRCode;
    EditText inputFooter,inputHarga,inputMemo;
    Button btnSimpan,btnBuat;
    RadioGroup radioWarna;
    String hexColor = "#FFFFFF";
    TextView txthasil;

    public QRcodeGeneratorFragment() {
        // Required empty public constructor
    }

    public static QRcodeGeneratorFragment newInstance(String isiQRCode) {
        QRcodeGeneratorFragment fragment = new QRcodeGeneratorFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, isiQRCode);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            isiQRCode = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_qrcode_generator, container, false);
        imageQRCode = view.findViewById(R.id.imageQRCode);
        btnSimpan = view.findViewById(R.id.btnSimpan);
        inputFooter = view.findViewById(R.id.inputFooter);
        inputHarga = view.findViewById(R.id.inputHarga);
        inputMemo = view.findViewById(R.id.inputMemo);
        btnBuat = view.findViewById(R.id.btnBuat);
        radioWarna = view.findViewById(R.id.radioWarna);
        txthasil = view.findViewById(R.id.txthasil);
        radioWarna.setOnCheckedChangeListener(this);

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bagikan();
            }
        });
        btnBuat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createQRcode();
                Utils.hideSoftKeyboard(getActivity());
            }
        });
        createQRcode();
        return view;
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        Drawable background = group.findViewById(checkedId).getBackground();
        if (background instanceof ColorDrawable) {
            int color = ((ColorDrawable) background).getColor();
            hexColor = String.format("#FF%06X", (0xFFFFFF & color));
            createQRcode();
        }
    }

    public void bagikan(){
        try {
            File cachePath = new File(getContext().getCacheDir(), "images");
            cachePath.mkdirs(); // don't forget to make the directory
            Bitmap bmp = ((BitmapDrawable) imageQRCode.getDrawable()).getBitmap();
            FileOutputStream stream = new FileOutputStream(cachePath + "/bagikan.png"); // overwrites this image every time
            bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
            stream.close();
            File newFile = new File(cachePath, "bagikan.png");
            Uri contentUri = FileProvider.getUriForFile(getContext(), "id.diruna.wallet.fileprovider", newFile);

            if (contentUri != null) {
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION); // temp permission for receiving app to read this file
                shareIntent.setDataAndType(contentUri, getContext().getContentResolver().getType(contentUri));
                shareIntent.putExtra(Intent.EXTRA_STREAM, contentUri);
                startActivity(Intent.createChooser(shareIntent, "Bagikan"));
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void createQRcode(){
        String isi = isiQRCode+"/"+inputHarga.getText().toString().trim()+"/"+inputMemo.getText().toString();
        txthasil.setText(isi);
        final RenderOption renderOption = new RenderOption();
        renderOption.setContent(isi); // content to encode
        renderOption.setSize(800); // size of the final QR code image
        //renderOption.setBorderWidth(0); // width of the empty space around the QR code
        //renderOption.setEcl(ErrorCorrectionLevel.L); // (optional) specify an error correction level
        //renderOption.setPatternScale(0.35f); // (optional) specify a scale for patterns
        //renderOption.setRoundedPatterns(true); // (optional) if true, blocks will be drawn as dots instead
        //renderOption.setClearBorder(false); // if set to true, the background will NOT be drawn on the border area
        Color color = new Color();
        // color.setLight(android.graphics.Color.parseColor(hexColor)); // for blank spaces
        color.setDark(0xFF000000);
        color.setBackground(android.graphics.Color.parseColor(hexColor));
        renderOption.setColor(color); // set a color palette for the QR code
        //renderOption.setBackground(background); // set a background, keep reading to find more about it

        Logo logo = new Logo();
        Bitmap icon = BitmapFactory.decodeResource(getContext().getResources(),
                R.drawable.koindiruna);
        logo.setBitmap(icon);
        logo.setBorderRadius(10); // radius for logo's corners
        logo.setBorderWidth(10); // width of the border to be added around the logo
        logo.setScale(0.3f); // scale for the logo in the QR code
        //logo.setClippingRect(new Rect(0, 0, 200, 200)); // crop the logo image before applying it to the QR code
        renderOption.setLogo(logo); // set a logo, keep reading to find more about it
        AwesomeQrRenderer.renderAsync(renderOption, new Function1<RenderResult, Unit>() {
            @Override
            public Unit invoke(RenderResult renderResult) {
                if(inputFooter.getText().toString().length()>1 && inputFooter.getText().toString().length()<17)
                    imageQRCode.setImageBitmap(Utils.drawTextToBitmap(getContext(),renderResult.getBitmap(), inputFooter.getText().toString()));
                else
                    imageQRCode.setImageBitmap(renderResult.getBitmap());
                return null;
            }
        }, new Function1<Exception, Unit>() {
            @Override
            public Unit invoke(Exception e) {
                return null;
            }
        });
    }


}
