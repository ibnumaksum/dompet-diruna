package id.diruna.wallet.barcode;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import id.diruna.wallet.R;

public class BarcodeGeneratorActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barcode_generator);
        Intent intent = getIntent();

        if(intent.hasExtra("isiQRCode"))
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container,QRcodeGeneratorFragment.newInstance(intent.getStringExtra("isiQRCode")))
                .commit();
        else finish();
    }
}
