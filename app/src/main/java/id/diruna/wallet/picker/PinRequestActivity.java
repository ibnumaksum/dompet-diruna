package id.diruna.wallet.picker;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import id.diruna.wallet.R;
import id.diruna.wallet.fungsi.Utils;

public class PinRequestActivity extends AppCompatActivity {
    EditText inputPIN;
    String pinHash;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pin_request);
        inputPIN = findViewById(R.id.inputPIN);
        this.pinHash = Utils.getPref(this).getString("pinHash",null);
    }

    public void tombolClick(View view){
        Button btn = (Button)view;
        if(btn.getText().toString().toLowerCase().equals("clear")){
            inputPIN.setText("");
        }else if(btn.getText().toString().toLowerCase().equals("ok")){
            if(inputPIN.getText().toString().length()<6){
                inputPIN.setError("Minimal 6 digit");
                return;
            }
            if(this.pinHash==null){
                this.pinHash = Utils.SHA256(inputPIN.getText().toString());
                Utils.showDialog("Ulangi isi PIN\n\nHanya untuk pertama kali menambah PIN",this);
                inputPIN.setText("");
                return;
            }else{
                Utils.log(pinHash);
                final String sha256 = Utils.SHA256(inputPIN.getText().toString());
                Utils.log(sha256);
                if(!pinHash.trim().equals(sha256.trim())){
                    new AlertDialog.Builder(this)
                            .setIcon(R.mipmap.ic_launcher)
                            .setTitle(R.string.app_name)
                            .setMessage("PIN Tidak sama dengan sebelumnya, Lanjutkan?")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = getIntent();
                                    Utils.getPref(PinRequestActivity.this).edit().putString("pinHash",sha256).apply();
                                    intent.putExtra("pin",inputPIN.getText().toString());
                                    setResult(RESULT_OK,intent);
                                    finish();
                                }
                            })
                            .setNegativeButton("tidak",null)
                            .show();
                }else{
                    Intent intent = getIntent();
                    Utils.getPref(this).edit().putString("pinHash",pinHash).apply();
                    intent.putExtra("pin",inputPIN.getText().toString());
                    setResult(RESULT_OK,intent);
                    finish();
                }
            }

        }else{
            inputPIN.append(btn.getText().toString());
        }

    }
}
