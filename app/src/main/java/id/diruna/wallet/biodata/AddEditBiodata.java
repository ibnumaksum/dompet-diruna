package id.diruna.wallet.biodata;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;

import id.diruna.wallet.BaseActivity;
import id.diruna.wallet.R;
import id.diruna.wallet.fungsi.Utils;
import id.diruna.wallet.objek.Pemilik;
import io.realm.Realm;

public class AddEditBiodata extends BaseActivity implements View.OnClickListener{
    ImageView imgAvatar,imgPicker;
    TextInputEditText inputNama,inputHP,inputEmail,inputURL;
    Button btnSimpan;
    byte[] bitmap;

    long idPemilik = 0L;
    Realm realm;
    private Pemilik pemilik;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_edit_biodata);
        imgAvatar = findViewById(R.id.imgAvatar);
        imgPicker = findViewById(R.id.imgPicker);
        inputNama = findViewById(R.id.inputNama);
        inputHP = findViewById(R.id.inputHP);
        inputEmail = findViewById(R.id.inputEmail);
        inputURL = findViewById(R.id.inputURL);
        btnSimpan = findViewById(R.id.btnSimpan);

        btnSimpan.setOnClickListener(this);
        imgPicker.setOnClickListener(this);

        Intent intent = getIntent();
        try {
            idPemilik = Long.parseLong(intent.getLongExtra("idPemilik",1L)+"",32);
        }catch (Exception e){}
        Utils.log("idPemilik: "+idPemilik);
        realm = Realm.getDefaultInstance();
        if(idPemilik>0) {
            pemilik = realm.where(Pemilik.class).equalTo("idPemilik",idPemilik).findFirst();// boxStore.boxFor(Pemilik.class).get(idPemilik);
            if (pemilik != null) {
                if(pemilik.foto!=null)
                    imgAvatar.setImageBitmap(BitmapFactory.decodeByteArray(pemilik.foto, 0, pemilik.foto.length));
                inputNama.setText(pemilik.namaPemilik);
                inputHP.setText(pemilik.nomorHP);
                inputEmail.setText(pemilik.email);
                inputURL.setText(pemilik.urlWeb);
            }else{
                Utils.log("Tidak ada user");
            }
        }
    }


    @Override
    public void onClick(View v) {
        if(v==imgPicker){
            try {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Pilih Gambar"), 33);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), "Gagal membuka File", Toast.LENGTH_LONG).show();
            }
        }else if(v == btnSimpan){
            if(inputNama.getText().toString().length()<2){
                inputNama.setError("Mohon isi nama lengkap");
                return;
            }
            boolean isNew = false;
            if(!realm.isInTransaction())
                realm.beginTransaction();

            if(pemilik==null) {
                isNew = true;

                //Tambah
                if (idPemilik == 1) {
                    pemilik = new Pemilik(1);
                } else {
                    pemilik = new Pemilik();
                }
            }else{
                pemilik = realm.where(Pemilik.class).equalTo("idPemilik",idPemilik).findFirst();
            }
            pemilik.namaPemilik = inputNama.getText().toString();
            pemilik.nomorHP = inputHP.getText().toString();
            pemilik.email = inputEmail.getText().toString();
            pemilik.urlWeb = inputURL.getText().toString();
            if(bitmap!=null){
                pemilik.foto = bitmap;
            }

            if(isNew){
                realm.copyToRealm(pemilik);
            }

            realm.commitTransaction();
            setResult(RESULT_OK);
            finish();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode==RESULT_OK){
            if(requestCode==33){
                Uri selectedImage = data.getData();
                Picasso.with(AddEditBiodata.this).load(selectedImage)
                        .resize(80,80)
                        .centerCrop()
                        .into(imgAvatar, new Callback() {
                            @Override
                            public void onSuccess() {
                                Bitmap bmp = ((BitmapDrawable) imgAvatar.getDrawable()).getBitmap();
                                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                                bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                                bitmap = baos.toByteArray();
                            }

                            @Override
                            public void onError() {
                                Utils.showToast("Gagal memilih gambar",AddEditBiodata.this);
                            }
                        });

            }
        }
    }
}
