package id.diruna.wallet.biodata;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import id.diruna.wallet.R;

public class BiodataActivity extends AppCompatActivity {
    long idPemilik;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_biodata);

        Intent intent = getIntent();
        if(intent.hasExtra("idPemilik")){
            idPemilik = intent.getLongExtra("idPemilik",0);
        }else{
            idPemilik = 0;
        }
    }
}
