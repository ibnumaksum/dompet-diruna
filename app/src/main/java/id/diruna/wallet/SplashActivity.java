package id.diruna.wallet;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import id.diruna.wallet.biodata.AddEditBiodata;
import id.diruna.wallet.objek.Pemilik;
import io.realm.Realm;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        cekBiodata();
    }

    private void cekBiodata(){
        Pemilik pemilik = Realm.getDefaultInstance().where(Pemilik.class).equalTo("idPemilik",1).findFirst();
        if(pemilik==null){
            //Buka add Pemilik
            Intent intent = new Intent(this,AddEditBiodata.class);
            intent.putExtra("idPemilik",1);
            startActivityForResult(intent,2);
            return;
        }else {
            startActivity(new Intent(this, MainActivity.class));
        }
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode==RESULT_OK){
            if(requestCode==2){
                cekBiodata();
            }
        }else
            finish();
    }
}
